/*
stripped down tree examples for DSA lecture 10
this one adds, removes and contains using breadth first search


 */
package generaltree;

import java.util.*;

/**
 *
 * @author ajb
 */
public class DepthFirstTree {
    TreeNode head;
    int height=0;
    boolean recursiveContains=false;
/*
Search through to find the node with the largest level      
    */    
    public int findHeight(){
        if(head==null)
          height=0;
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        while(!q.isEmpty()){
            TreeNode temp=q.remove();
            height=temp.level;
            for(int i=0;i<temp.numOffspring;i++)
                q.add(temp.offspring[i]);
        }
      
        return height;
    }
/*
    Do a breadth first search to find the first empty place. Assum a maximum
    number of nodes for adding and removal purposes, defined in the node
    
    Normally you would add with some explicit criteria. A Tree is not a stucture
    you just chuck stuff in, otherwise whats the point? There always has to be
    some implied structure
    */    
    /*
    Do a breadth first search to find the first empty place. Assum a maximum
    number of nodes for adding and removal purposes, defined in the node
    
    Normally you would add with some explicit criteria. A Tree is not a stucture
    you just chuck stuff in, otherwise whats the point? There always has to be
    some implied structure
    */    
    public void add(Object obj){
        TreeNode toAdd=new TreeNode(obj);
//        System.out.println("adding "+obj);
        if(head==null){
            head=toAdd;
            return;
        }
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        boolean found=false;
        while(!found){
//Get node    
            TreeNode temp=q.remove();
            if(temp.isFull()){//Add offspring to process queue
                for(int i=0;i<temp.numOffspring;i++)
                    q.add(temp.offspring[i]);
            }
            else{ //Space found, add in
                toAdd.setLevel(temp.level+1);
                System.out.println("Adding "+obj+" at level "+(temp.level+1));
                temp.add(toAdd);
                found = true;
            }
        }
            
    }

/**
	Stack s:= empty TreeNode stack
//push head
	s.push(head)
	while s.isEmpty()=false
//get next element to examine
		temp:=q.peek()
		if(temp.notSeen() and temp.isEqual(E))
			return true
//Get next unseen offspring		
	x:=temp.getNextOffspring()
	if(x=null)
		s.pop()
	else
		s.push(x)
	return false
 */   
    public boolean contains(Object obj){
        if(recursiveContains)
            return contains(obj,head);
        if(head==null)
            return false;
        Stack<TreeNode> s= new Stack<>();
        HashSet<TreeNode> seen=new HashSet<>(); 
        s.push(head);
        while(!s.isEmpty()){
            TreeNode temp=s.peek();
            if(temp.obj.equals(obj)) 
                return true;
            seen.add(temp);
//Find next offspring that has not been seen. 
//This is an inefficient way of doing it, better to internally store the 
//position of last seen in the node
            int pos=0;
            boolean found=false;
            while(!found && pos<temp.numOffspring){
                if(seen.contains(temp.offspring[pos]))
                    pos++;
                else
                    found=true;
            }
            if(!found)//Pop the current node, nothing left to examine
                s.pop();
            else
                s.push(temp.offspring[pos]);
        }
        return false;
    }
      public boolean contains(Object obj, TreeNode t){
//          System.out.println("Looking at node "+t);
        if(t==null)
            return false;
        if(t.obj.equals(obj))
            return true;
        TreeNode[] children=t.offspring;
        for(int i=0;i<t.numOffspring;i++){
            if(contains(obj,children[i])) 
                return true;
        }
        return false;
    }
  
    public String printAsTree(){
        if(head==null)
            return "EMPTY TREE";
        
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        String str="";
        int level=0;
        while(!q.isEmpty()){
            TreeNode temp=q.remove();
            for(int i=0;i<temp.numOffspring;i++)
                q.add(temp.offspring[i]);
            str+=temp.obj+",";
            if(temp.level!=level){
                str+="\n";
                level=temp.level;
            }
                
        }
        return str;

        
    }
    public String toString(){
        if(head==null)
            return "EMPTY TREE";
        String str="";
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        while(!q.isEmpty()){
            TreeNode temp=q.remove();
            for(int i=0;i<temp.numOffspring;i++)
                q.add(temp.offspring[i]);
            str+=temp.obj+","+temp.level+"\n";
        }
        return str;
    }


    public static void main(String[] args) {
            DepthFirstTree myTree= new DepthFirstTree();
                myTree.add("A");
                myTree.add("B");
                myTree.add("C");
                myTree.add("D");
                myTree.add("E");
                myTree.add("F");
                myTree.add("G");
                myTree.add("H");
                myTree.add("I");
            System.out.println("MY TREE:\n "+myTree.printAsTree());
              
            System.out.println("MY TREE contains A? :\n "+myTree.contains("A"));
            System.out.println("MY TREE contains E? :\n "+myTree.contains("E"));

            System.out.println("MY TREE contains I? :\n "+myTree.contains("I"));
            System.out.println("MY TREE contains X? :\n "+myTree.contains("X"));
            
    }

}
