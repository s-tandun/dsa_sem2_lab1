/*
stripped down tree examples for DSA lecture 10
this one adds, removes and contains using breadth first search


 */
package generaltree;

import java.util.*;

/**
 *
 * @author ajb
 */
public class BreadthFirstTree {
    TreeNode head;
    int height=0;
/*
Search through to find the node with the largest level      
    */    
    public int findHeight(){
        if(head==null)
          height=0;
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        while(!q.isEmpty()){
            TreeNode temp=q.remove();
            height=temp.level;
            for(int i=0;i<temp.numOffspring;i++)
                q.add(temp.offspring[i]);
        }
      
        return height;
    }
/*
    Do a breadth first search to find the first empty place. Assum a maximum
    number of nodes for adding and removal purposes, defined in the node
    
    Normally you would add with some explicit criteria. A Tree is not a stucture
    you just chuck stuff in, otherwise whats the point? There always has to be
    some implied structure
    */    
    public void add(Object obj){
        TreeNode toAdd=new TreeNode(obj);
//        System.out.println("adding "+obj);
        if(head==null){
            head=toAdd;
            return;
        }
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        boolean found=false;
        while(!found){
//Get node    
            TreeNode temp=q.remove();
            if(temp.isFull()){//Add offspring to process queue
                for(int i=0;i<temp.numOffspring;i++)
                    q.add(temp.offspring[i]);
            }
            else{ //Space found, add in
                toAdd.setLevel(temp.level+1);
                System.out.println("Adding "+obj+" at level "+(temp.level+1));
                temp.add(toAdd);
                found = true;
            }
        }
            
    }
    
/**
 * remove needs to do a bit more work, and more clear definition. When we remove 
 * a node we need to rewire it and decide what to do with any offspring
 * 
 * Even removing the whole subtree is difficult with a BFS. It is much easier 
 * to do this with a DFS search, as the parents are in the stack in order.
 * @param obj
 * @return 
 */    
    public boolean remove(Object obj){
        return false;
    }
    public boolean contains(Object obj){
        if(head==null)
            return false;
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        int check=1;
        while(!q.isEmpty()){
            TreeNode temp=q.remove();
            System.out.print("("+check+","+temp.obj+"), ");
            check++;
            if(temp.obj.equals(obj)) 
                return true;
            for(int i=0;i<temp.numOffspring;i++)
                q.add(temp.offspring[i]);
        }
        return false;
    }
    public String printAsTree(){
        if(head==null)
            return "EMPTY TREE";
        
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        String str="";
        int level=0;
        while(!q.isEmpty()){
            TreeNode temp=q.remove();
            for(int i=0;i<temp.numOffspring;i++)
                q.add(temp.offspring[i]);
            str+=temp.obj+",";
            if(temp.level!=level){
                str+="\n";
                level=temp.level;
            }
                
        }
        return str;

        
    }
    public String toString(){
        if(head==null)
            return "EMPTY TREE";
        String str="";
        Queue<TreeNode> q= new LinkedList<>();
        q.add(head);
        while(!q.isEmpty()){
            TreeNode temp=q.remove();
            for(int i=0;i<temp.numOffspring;i++)
                q.add(temp.offspring[i]);
            str+=temp.obj+","+temp.level+"\n";
        }
        return str;
    }


    public static void main(String[] args) {
            BreadthFirstTree myTree= new BreadthFirstTree();
            for(int i=0;i<20;i++)
                myTree.add(i);
            System.out.println("MY TREE:\n "+myTree.printAsTree());
              
            System.out.println("MY TREE contains 9? :\n "+myTree.contains(9));
            System.out.println("MY TREE contains 19? :\n "+myTree.contains(19));

            System.out.println("MY TREE contains 2000? :\n "+myTree.contains(2000));
            
    }

}
